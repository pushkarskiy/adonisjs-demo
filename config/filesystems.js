'use strict'

const Helpers = use('Helpers')

module.exports = {

  default: 'public',

  public: {
    root: Helpers.publicPath('uploads'),
    options: {
      encoding: 'utf8'
    }
  },

  views: {
    root: Helpers.viewsPath(),
    options: {
      encoding: 'utf8'
    }
  }

}
