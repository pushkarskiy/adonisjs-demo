'use strict'
/* global $:false; tinymce:false, monaco:false */

/* Handle Sidebar - Menu
 ------------------------------------------------ */
const handleSidebarMenu = function () {
  $('.sidebar .nav > .has-sub > a').click(function () {
    const target = $(this).next('.sub-menu')
    const otherMenu = '.sidebar .nav > li.has-sub > .sub-menu'

    if ($('.page-sidebar-minified').length === 0) {
      $(otherMenu).not(target).slideUp(250, function () {
        $(this).closest('li').removeClass('expand')
      })
      $(target).slideToggle(250, function () {
        const targetLi = $(this).closest('li')
        if ($(targetLi).hasClass('expand')) {
          $(targetLi).removeClass('expand')
        } else {
          $(targetLi).addClass('expand')
        }
      })
    }
  })
  $('.sidebar .nav > .has-sub .sub-menu li.has-sub > a').click(function () {
    if ($('.page-sidebar-minified').length === 0) {
      const target = $(this).next('.sub-menu')
      $(target).slideToggle(250)
    }
  })
}

/* Handle Sidebar - Minify / Expand
 ------------------------------------------------ */
const handleSidebarMinify = function () {
  $('[data-click=sidebar-minify]').click(function (e) {
    e.preventDefault()
    const sidebarClass = 'page-sidebar-minified'
    const targetContainer = '#page-container'
    if ($(targetContainer).hasClass(sidebarClass)) {
      $(targetContainer).removeClass(sidebarClass)
    } else {
      $(targetContainer).addClass(sidebarClass)
      if ($(targetContainer).hasClass('page-sidebar-fixed')) {
        $('#sidebar [data-scrollbar="true"]').slimScroll({destroy: true})
        $('#sidebar [data-scrollbar="true"]').removeAttr('style')
      }
      // firefox bugfix
      $('#sidebar [data-scrollbar=true]').trigger('mouseover')
    }
    $(window).trigger('resize')
  })
}

/* Handle TinyMCE
 ------------------------------------------------ */
const handleTinyMCE = function () {
  tinymce.init({
    selector: '.js-text-editor',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
    ],
    toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
    image_advtab: true
  })
}

/* Handle TemplateEditor
 ------------------------------------------------ */
const handleTemplateEditor = function () {
  var $container = $('#monaco_container')

  if ($container.length) {
    var content = $container.text()
    $container.empty()

    require.config({paths: {'vs': '/monaco/min/vs'}})
    require(['vs/editor/editor.main'], function () {
      window.MonakoEditor = monaco.editor.create(document.getElementById('monaco_container'), {
        language: 'html',
        value: content,
        fontSize: 14,
        theme: 'vs-dark'
      })
    })

    $container.show()

    window.onresize = function () {
      if (window.MonakoEditor) {
        window.MonakoEditor.layout()
      }
    }

    $('.template_form').on('submit', function (e) {
      e.preventDefault()
      var $form = $(this)
      var data = {
        _csrf: $form.find('[name="_csrf"]').val(),
        file: window.MonakoEditor.getValue()
      }
      $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        dataType: 'json',
        data: data,
        success: function (response) {
          if (response && response.success) {
            $.gritter.add({
              title: 'Успешно сохранено!'
            })
          } else {
            $.gritter.add({
              title: 'Ошибка попробуйте позднее!'
            })
          }
        }
      })
    })
  }
}

window.App = function () {
  return {
    init: function () {
      handleSidebarMenu()
      handleSidebarMinify()
      handleTinyMCE()
      handleTemplateEditor()
    }
  }
}()
