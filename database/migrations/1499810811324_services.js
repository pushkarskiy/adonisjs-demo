'use strict'

const Schema = use('Schema')

class ServicesTableSchema extends Schema {
  up () {
    this.create('services', (table) => {
      table.increments()
      table.timestamps()
      table.string('name')
      table.text('text')
      table.string('desc')
      table.integer('price')
      table.integer('support_price')
    })
  }
  down () {
    this.drop('services')
  }
}

module.exports = ServicesTableSchema
