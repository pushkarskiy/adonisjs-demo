'use strict'

const Schema = use('Schema')

class OrdersTableSchema extends Schema {
  up () {
    this.table('orders', (table) => {
      table.enu('type', ['call', 'promo', 'service', 'portfolio'])
      table.integer('service_id')
      table.integer('portfolio_id')
    })
  }
  down () {
    this.table('orders', (table) => {
      table.dropColumn('service')
    })
  }
}

module.exports = OrdersTableSchema
