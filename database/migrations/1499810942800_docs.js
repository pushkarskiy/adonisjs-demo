'use strict'

const Schema = use('Schema')

class DocsTableSchema extends Schema {
  up () {
    this.create('docs', (table) => {
      table.increments()
      table.timestamps()
      table.string('title')
      table.string('url')
    })
  }

  down () {
    this.drop('docs')
  }
}

module.exports = DocsTableSchema
