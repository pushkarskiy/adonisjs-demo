'use strict'

const Lucid = use('Lucid')

class Customer extends Lucid {
  static get rules () {
    return {
      name: 'required',
      site: 'required',
      email: 'email',
      budget: 'required',
      support_price: 'required'
    }
  }
}

module.exports = Customer
