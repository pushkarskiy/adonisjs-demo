'use strict'

const Lucid = use('Lucid')
const Hash = use('Hash')

class User extends Lucid {

  apiTokens () {
    return this.hasMany('App/Model/Token')
  }

  static get rules () {
    return {
      email: 'required|email|unique:users',
      password: 'required',
    }
  }

}

module.exports = User
