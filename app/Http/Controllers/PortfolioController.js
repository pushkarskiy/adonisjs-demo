'use strict'

const Helpers = use('Helpers')
const Portfolio = use('App/Model/Portfolio')
const Validator = use('Validator')
const FILE_CONFIG = { maxSize: '2mb', allowedExtensions: ['jpg', 'png', 'jpeg'] }

class PortfolioController {
  * add (request, response) {
    const form = request.only('name', 'budget', 'views', 'visitors')
    const validation = yield Validator.validate(form, Portfolio.rules)

    if (validation.fails()) {
      yield request.withAll().andWith({errors: validation.messages()}).flash()
      response.redirect('back')
    } else {
      const img = request.file('img', FILE_CONFIG)
      const fileName = `${new Date().getTime()}.${img.extension()}`
      yield img.move(Helpers.publicPath('assets/upload'), fileName)

      if (!img.moved()) {
        yield request.withAll().andWith({errors: img.errors()}).flash()
        response.redirect('back')
        return
      }

      form.img = '/assets/upload/' + img.uploadName()

      yield Portfolio.create(form)
      response.redirect('/admin/portfolio')
    }
  }
  * delete (request, response) {
    const id = request.only('id').id
    const portfolio = yield Portfolio.query().where('id', id).first()

    if (portfolio) {
      yield portfolio.delete()
      yield request.withAll().andWith({success: 'Успешно удалена'}).flash()
    } else {
      yield request.withAll().andWith({
        errors: [{message: 'Не удалось удалить'}]
      }).flash()
    }
    response.redirect('back')
  }
  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const portfolios = yield Portfolio.all()
      const data = {
        portfolios: portfolios.toJSON()
      }

      yield response.sendView('pages.portfolio.list', data)
    }
  }
}

module.exports = PortfolioController
