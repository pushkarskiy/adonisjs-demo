'use strict'

const Doc = use('App/Model/Doc')
const Portfolio = use('App/Model/Portfolio')
const Service = use('App/Model/Service')
const Order = use('App/Model/Order')
const Validator = use('Validator')
const Event = use('Event')

class SiteController {
  * index (request, response) {
    const docs = yield Doc.all()
    const portfolios = yield Portfolio.all()
    const services = yield Service.all()
    const data = {
      docs: docs.toJSON(),
      portfolios: portfolios.toJSON(),
      services: services.toJSON()
    }
    yield response.sendView('site.index', data)
  }
  * order (request, response) {
    const form = request.only('name', 'phone', 'email', 'type', 'service_id', 'portfolio_id')
    const validation = yield Validator.validate(form, Order.rules)

    if (validation.fails()) {
      yield response.json({ success: false, errors: validation.messages() })
    } else {
      let order = yield Order.create(form)
      let service = order.service_id ? yield SiteController.getService(order.service_id) : false
      let portfolio = order.portfolio_id ? yield SiteController.getPortfolio(order.portfolio_id) : false

      const data = yield {
        order: order.toJSON(),
        service: service,
        portfolio: portfolio,
        promo: order.type === 'promo',
        call: order.type === 'call'
      }

      Event.fire('site.order', data)
      yield response.json({ success: true })
    }
  }
  static * getService (id) {
    let service = yield Service.query().where('id', id).first()
    return service.toJSON()
  }
  static * getPortfolio (id) {
    let portfolio = yield Portfolio.query().where('id', id).first()
    return portfolio.toJSON()
  }
}

module.exports = SiteController
