'use strict'

const Validator = use('Validator')
const Database = use('Database')
const Order = use('App/Model/Order')

class OrderController {
  * add (request, response) {
    const form = request.only('name', 'phone', 'service', 'email')
    const validation = yield Validator.validate(form, Order.rules)

    if (validation.fails()) {
      yield request.withAll().andWith({errors: validation.messages()}).flash()
      response.redirect('back')
    } else {
      yield Order.create(form)
      response.redirect('/admin/order')
    }
  }
  * delete (request, response) {
    const id = request.only('id').id
    const order = yield Order.query().where('id', id).first()

    if (order) {
      yield order.delete()
      yield request.withAll().andWith({success: 'Успешно удалена'}).flash()
    } else {
      yield request.withAll().andWith({
        errors: [{message: 'Не удалось удалить'}]
      }).flash()
    }
    response.redirect('back')
  }
  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      let orders = yield Database.table('orders')
      let portfolios = yield Database.table('portfolios')
      let services = yield Database.table('services')

      const data = {
        orders: orders,
        portfolios: portfolios,
        services: services
      }

      yield response.sendView('pages.order.list', data)
    }
  }
}

module.exports = OrderController
