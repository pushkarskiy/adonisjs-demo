'use strict'

const Customer = use('App/Model/Customer')
const Validator = use('Validator')

class CustomerController {
  * add (request, response) {
    const form = request.only('name', 'site', 'phone', 'email', 'budget', 'support_price', 'payment_day')
    const validation = yield Validator.validate(form, Customer.rules)

    if (validation.fails()) {
      yield request.withAll().andWith({errors: validation.messages()}).flash()
      response.redirect('back')
    } else {
      yield Customer.create(form)
      response.redirect('/admin/customer')
    }
  }
  * delete (request, response) {
    const id = request.only('id').id
    const customer = yield Customer.query().where('id', id).first()

    if (customer) {
      yield customer.delete()
      yield request.withAll().andWith({success: 'Успешно удалена'}).flash()
    } else {
      yield request.withAll().andWith({
        errors: [{message: 'Не удалось удалить'}]
      }).flash()
    }
    response.redirect('back')
  }
  * list (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const customers = yield Customer.all()
      const data = {
        customers: customers.toJSON()
      }

      yield response.sendView('pages.customer.list', data)
    }
  }
}

module.exports = CustomerController
