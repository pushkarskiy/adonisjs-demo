'use strict'

const Database = use('Database')

class AdminController {
  * index (request, response) {
    const isLoggedIn = yield request.auth.check()
    if (!isLoggedIn) {
      yield response.redirect('/auth/login')
    } else {
      const orders = yield Database.select('id').from('orders')
      const customers = yield Database.select('id').from('customers')
      const data = {
        orders,
        customers
      }
      yield response.sendView('pages.admin', data)
    }
  }
}

module.exports = AdminController
